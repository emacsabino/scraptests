from urllib.request import urlopen
from bs4 import BeautifulSoup

def pagina(url):

    html = urlopen(url)
    bsObj = BeautifulSoup(html)
    nameList = bsObj.findAll("span",{"class":"green"})
    for name in nameList:
        print(name.get_text())

def pagina2(url):

        html = urlopen(url)
        bsObj = BeautifulSoup(html)
        nameList = bsObj.findAll("img")
        for name in nameList:
            print(name)

def pagina3(url):

        html = urlopen(url)
        bsObj = BeautifulSoup(html)
        nameList = bsObj.findAll("a",text="Principal")
        print(nameList)

#pagina("http://www.pythonscraping.com/pages/warandpeace.html")
#pagina2("https://www.facebook.com/andressabelford/media_set?set=a.187310097973049.34246.100000821812324&type=3")
pagina3("http://www.diariooficial.pi.gov.br/")